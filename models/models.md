﻿## Models

### CNN classifier

The model is inspired by [VoxNet: a 3D CNN for real-time object recognition](http://dimatura.net/publications/voxnet_maturana_scherer_iros15.pdf).

![VoxNet-based CNN](cnn_dis_201711_verify.png "Classifier")

For cross-validation, two models were trained: one on Nove 2017, and another one on Dec 2017 observations.

#### Training data

201711
Total number of examples in the dataset  25532
-1 Unknown 0 (0.0%)
0 Solar Wind 8126 (31.8%)
1 Foreshock 5954 (23.3%)
2 Magnetosheath 5306 (20.8%)
3 Magnetosphere 6146 (24.1%)

201712
Total number of examples in the dataset  25275
-1 Unknown 0 (0.0%)
0 Solar Wind 6305 (24.9%)
1 Foreshock 6601 (26.1%)
2 Magnetosheath 5612 (22.2%)
3 Magnetosphere 6757 (26.7%)
